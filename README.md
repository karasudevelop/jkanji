<img style="width:128px;float:left" src="https://github.com/karasudevelop/JKanji/blob/master/logo.png?raw=true"/>
# **JKanji**

### Memorize kanjis e palavras japoneses.

<img style="float:left" src="https://github.com/karasudevelop/JKanji/blob/master/1.png?raw=true"/>
<img style="float:left" src="https://github.com/karasudevelop/JKanji/blob/master/2.png?raw=true"/>
<img style="float:left" src="https://github.com/karasudevelop/JKanji/blob/master/3.png?raw=true"/>
<img style="float:left" src="https://github.com/karasudevelop/JKanji/blob/master/4.png?raw=true"/>
<img style="float:left" src="https://github.com/karasudevelop/JKanji/blob/master/5.png?raw=true"/>
