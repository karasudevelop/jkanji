package com.karasudevelop.jkanji;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

//http://stackoverflow.com/questions/11295080/android-wrap-content-is-not-working-with-listview
public class WrappedListView extends ListView
{
    public WrappedListView( Context context )
    {
        super( context );
    }

    public WrappedListView( Context context, AttributeSet attrs )
    {
        super( context, attrs );
    }

    public WrappedListView( Context context, AttributeSet attrs, int defStyleAttr )
    {
        super( context, attrs, defStyleAttr );
    }

    @Override
    public void onMeasure( int widthMeasureSpec, int heightMeasureSpec )
    {
        int expandSpec = MeasureSpec.makeMeasureSpec( Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST );
        super.onMeasure( widthMeasureSpec, expandSpec );
    }
}
