package com.karasudevelop.jkanji;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.liulishuo.filedownloader.FileDownloader;

import java.io.File;

public class App extends Application
{
    private static Context context;
    private static File folder;
    private static SharedPreferences preferences;
    private static FontManager fontManager;

    public final static String JLPT_FILTER = "JLPT_FILTER";
    public final static String GRADE_FILTER = "GRADE_FILTER";

    @Override
    public void onCreate()
    {
        super.onCreate();

        FileDownloader.init( this );

        context = getBaseContext();

        folder = new File( Environment.getExternalStorageDirectory(), "JKanji" );
        folder.mkdirs();

        preferences = PreferenceManager.getDefaultSharedPreferences( context );

        fontManager = new FontManager( this );
    }

    public static Context context()
    {
        return context;
    }

    public static File appFolder()
    {
        return folder;
    }

    public static SharedPreferences preferences()
    {
        return preferences;
    }

    public static SharedPreferences.Editor editor()
    {
        return preferences.edit();
    }

    public static FontManager fonts()
    {
        return fontManager;
    }

    public static void show( Object object )
    {
        show( object, false );
    }

    public static void show( Object object, boolean longDuration )
    {
        Toast.makeText( context, object.toString(), longDuration ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT ).show();
    }

    public static Resources resources()
    {
        return context().getResources();
    }

    public static String version()
    {
        return resources().getString( R.string.app_name ) +
               "\nVersão: " + BuildConfig.VERSION_NAME +
               "\nVersão do banco de dados: " + Jisho.versao() +
               "\n\nCopyright 2016, karasudevelop";
    }
}
