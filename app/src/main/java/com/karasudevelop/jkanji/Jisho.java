package com.karasudevelop.jkanji;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Jisho
{
    public Jisho()
    {
    }

    public static File filename()
    {
        return new File( App.appFolder(), "jisho.db" );
    }

    public static boolean exists()
    {
        return filename().exists();
    }

    private static SQLiteDatabase open()
    {
        File arquivo = filename();

        if( !arquivo.exists() ) return null;

        return SQLiteDatabase.openDatabase(
                arquivo.getAbsolutePath(),
                null,
                SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READONLY );
    }

    public static long versao()
    {
        long versao = -1;

        SQLiteDatabase db = open();

        if( db != null && db.isOpen() )
        {
            try
            {
                Cursor cursor = db.rawQuery( "SELECT  * FROM info WHERE nome = 'versao'", null );
                if( cursor != null && cursor.moveToFirst() )
                {
                    versao = Long.parseLong( cursor.getString( 1 ) );
                    cursor.close();
                }
            }
            catch( Exception e )
            {
                e.printStackTrace();
            }
            finally
            {
                db.close();
            }
        }

        return versao;
    }

    public static List<Kanji> kanjis( String search, int jlpt, int grade )
    {
        Log.i( "TAG", "Obtendo a lista de kanjis." );

        List<Kanji> result = new ArrayList<>();

        SQLiteDatabase db = open();

        if( db != null && db.isOpen() )
        {
            if( search != null )
            {
                search = search.toLowerCase();
            }

            try
            {
                Cursor cursor = db.rawQuery(
                        "SELECT  * FROM kanjis WHERE ( " +
                        "( grade IN (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ) AND " +
                        "( jlpt IN (?, ?, ?, ?, ?, ?) ) ) ORDER BY freq ASC",
                        new String[]{
                                grade == 0 || (grade & 1) != 0 ? "1" : "0",
                                grade == 0 || (grade & 2) != 0 ? "2" : "0",
                                grade == 0 || (grade & 4) != 0 ? "3" : "0",
                                grade == 0 || (grade & 8) != 0 ? "4" : "0",
                                grade == 0 || (grade & 16) != 0 ? "5" : "0",
                                grade == 0 || (grade & 32) != 0 ? "6" : "0",
                                grade == 0 || (grade & 64) != 0 ? "7" : "0",
                                grade == 0 || (grade & 64) != 0 ? "8" : "0",
                                grade == 0 || (grade & 128) != 0 ? "9" : "0",
                                grade == 0 || (grade & 256) != 0 ? "10" : "0",
                                grade == 0 ? "9999" : "0",
                                //JLPT
                                jlpt == 0 || (jlpt & 1) != 0 ? "5" : "9",
                                jlpt == 0 || (jlpt & 2) != 0 ? "4" : "9",
                                jlpt == 0 || (jlpt & 4) != 0 ? "3" : "9",
                                jlpt == 0 || (jlpt & 8) != 0 ? "2" : "9",
                                jlpt == 0 || (jlpt & 16) != 0 ? "1" : "9",
                                jlpt == 0 ? "0" : "9"
                        } );

                if( cursor != null && cursor.moveToFirst() )
                {
                    do
                    {
                        String significado = cursor.getString( cursor.getColumnIndex( "significado" ) );
                        String c = cursor.getString( cursor.getColumnIndex( "kanji" ) );

                        if( (search == null || search.length() == 0 ||
                             c == search ||
                             significado.toLowerCase().contains( search ) ||
                             cursor.getString( cursor.getColumnIndex( "kanji" ) ) == search) )
                        {

                            Kanji kanji = new Kanji( c,
                                                     cursor.getString( cursor.getColumnIndex( "on" ) ),
                                                     cursor.getString( cursor.getColumnIndex( "kun" ) ),
                                                     cursor.getString( cursor.getColumnIndex( "nanori" ) ),
                                                     cursor.getInt( cursor.getColumnIndex( "jlpt" ) ),
                                                     cursor.getInt( cursor.getColumnIndex( "grade" ) ),
                                                     cursor.getInt( cursor.getColumnIndex( "freq" ) ),
                                                     cursor.getInt( cursor.getColumnIndex( "tracos" ) ),
                                                     significado );

                            result.add( kanji );
                        }
                    } while( cursor.moveToNext() );

                    cursor.close();
                }

                if( App.preferences().getBoolean(
                        App.resources().getString( R.string.settings_random_get_kanjis ), true ) )
                {
                    Collections.shuffle( result, new Random( System.nanoTime() ) );
                }
            }
            catch( Exception e )
            {
                e.printStackTrace();
            }
            finally
            {
                db.close();
            }
        }

        return result;
    }

    public static Kanji kanji( String c )
    {
        SQLiteDatabase db = open();

        if( db != null && db.isOpen() )
        {
            try
            {
                Cursor cursor = db.rawQuery( "SELECT  * FROM kanjis WHERE ( kanji = ? )", new String[]{ c } );

                if( cursor != null && cursor.moveToFirst() )
                {
                    Kanji kanji = new Kanji( cursor.getString( cursor.getColumnIndex( "kanji" ) ),
                                             cursor.getString( cursor.getColumnIndex( "on" ) ),
                                             cursor.getString( cursor.getColumnIndex( "kun" ) ),
                                             cursor.getString( cursor.getColumnIndex( "nanori" ) ),
                                             cursor.getInt( cursor.getColumnIndex( "jlpt" ) ),
                                             cursor.getInt( cursor.getColumnIndex( "grade" ) ),
                                             cursor.getInt( cursor.getColumnIndex( "freq" ) ),
                                             cursor.getInt( cursor.getColumnIndex( "tracos" ) ),
                                             cursor.getString( cursor.getColumnIndex( "significado" ) ) );
                    cursor.close();
                    db.close();

                    return kanji;
                }
            }
            catch( Exception e )
            {
                e.printStackTrace();
            }
            finally
            {
                db.close();
            }
        }

        return null;
    }

    public static List<Kotoba> kotobas( String kanji, int limit )
    {
        List<Kotoba> res = new ArrayList<>();

        SQLiteDatabase db = open();

        if( db != null && db.isOpen() )
        {
            try
            {
                Cursor cursor;
                if( App.preferences().getBoolean(
                        App.resources().getString( R.string.settings_show_kanji_ext ), true ) )
                    cursor = db.rawQuery( "SELECT  * FROM kotobas WHERE ( kanji LIKE '%" + kanji + "%' OR escrita LIKE '%" + kanji + "%' ) ORDER BY jlpt DESC, leitura ASC", null );
                else
                    cursor = db.rawQuery( "SELECT  * FROM kotobas WHERE ( kanji LIKE '%" + kanji + "%' ) ORDER BY jlpt DESC, leitura ASC", null );

                if( cursor != null && cursor.moveToFirst() )
                {
                    if( limit < 0 )
                    {
                        limit = Integer.MAX_VALUE;
                    }

                    do
                    {
                        Kotoba kotoba = new Kotoba( cursor.getString( cursor.getColumnIndex( "kanji" ) ), //kotoba
                                                    cursor.getString( cursor.getColumnIndex( "escrita" ) ), //outras formas de escrita
                                                    cursor.getString( cursor.getColumnIndex( "leitura" ) ), //kana
                                                    cursor.getInt( cursor.getColumnIndex( "jlpt" ) ), //jlpt
                                                    cursor.getString( cursor.getColumnIndex( "significado" ) ) );
                        res.add( kotoba );
                        limit--;
                    } while( limit > 0 && cursor.moveToNext() );

                    cursor.close();
                }
            }
            catch( Exception e )
            {
                e.printStackTrace();
            }
            finally
            {
                db.close();
            }
        }

        return res;
    }

    public static List<Wabun> wabuns( String kotoba )
    {
        List<Wabun> res = new ArrayList<>();

        SQLiteDatabase db = open();

        if( db != null && db.isOpen() )
        {
            try
            {

                Cursor cursor = db.rawQuery( "SELECT  * FROM wabun WHERE ( kanji LIKE '%" + kotoba + "%' )", null );

                if( cursor != null && cursor.moveToFirst() )
                {
                    do
                    {
                        Wabun sentenca = new Wabun( cursor.getString( 1 ), //kanji
                                                    cursor.getString( 2 ) ); //eigo
                        res.add( sentenca );
                    } while( cursor.moveToNext() );

                    cursor.close();
                }
            }
            catch( Exception e )
            {
                e.printStackTrace();
            }
            finally
            {
                db.close();
            }
        }

        return res;
    }

    public static Kotowaza kotowaza()
    {
        SQLiteDatabase db = open();

        if( db != null && db.isOpen() )
        {
            try
            {

                Cursor cursor = db.rawQuery( "SELECT  * FROM kotowaza", null );

                if( cursor != null )
                {
                    int qtde = cursor.getCount();
                    int dia = Calendar.getInstance().get( Calendar.DAY_OF_YEAR );

                    if( cursor.move( dia % qtde ) )
                    {
                        Kotowaza kotowaza = new Kotowaza( cursor.getString( 1 ),
                                                          cursor.getString( 2 ),
                                                          cursor.getString( 3 ) );
                        cursor.close();
                        db.close();

                        return kotowaza;
                    }
                }
            }
            catch( Exception e )
            {
                e.printStackTrace();
            }
            finally
            {
                db.close();
            }
        }

        return null;
    }
}
