package com.karasudevelop.jkanji;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.liulishuo.filedownloader.BaseDownloadTask;
import com.liulishuo.filedownloader.FileDownloadSampleListener;
import com.liulishuo.filedownloader.FileDownloader;

import java.util.List;

public class MainActivity extends BaseActivity implements
                                               NavigationView.OnNavigationItemSelectedListener
{
    private ViewPager pager;
    private NavigationView navLeft;
    private TextView kanjiCount;
    private EditText searchBox;

    private MenuItem favButton,
            searchButton,
            updateButton;

    private boolean searchBoxIsVisible = false;
    private String searchText = null;
    private boolean doubleClick = false;

    private KanjiPagerAdapter adapter;
    private Favoritos favoritos;

    private static final String[] servers = new String[]
            {
                    "https://github.com/karasudevelop/JKanji/blob/master/jisho.db?raw=true",
                    "https://www.dropbox.com/s/hu9bbck2898r3wl/jisho.db?dl=1"
            };

    private static final int SETTINGS_PREFS_RC = 1234;
    private static final String UPDATE_URL = "https://github.com/karasudevelop/JKanji/blob/master/update?raw=true";

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.main_activity );

        favoritos = new Favoritos( this );

        ActionBar actionBar = getSupportActionBar();

        if( actionBar != null )
        {
            actionBar.setTitle( "Kanjis" );
            actionBar.setHomeAsUpIndicator( R.drawable.menu_white );
            actionBar.setDisplayHomeAsUpEnabled( true );
        }

        pager = (ViewPager) findViewById( R.id.pager );
        navLeft = (NavigationView) findViewById( R.id.nav_left );

        navLeft.setNavigationItemSelectedListener( this );
        navLeft.setItemIconTintList( null );

        adapter = new KanjiPagerAdapter( getSupportFragmentManager() );

        pager.addOnPageChangeListener( new ViewPager.SimpleOnPageChangeListener()
        {
            @Override
            public void onPageSelected( int position )
            {
                updateFavoriteIcon();
            }
        } );

        pager.setAdapter( adapter );

        showProverbOfDay();

        if( !Jisho.exists() || Jisho.versao() == -1 )
        {
            App.show( "É necessário baixar o banco de dados." );
            App.show( "O download será iniciado.", true );

            int repo = Integer.parseInt( App.preferences().getString(
                    App.resources().getString( R.string.settings_choose_database_repo ), "0" ) );
            FileDownloader.getImpl().
                    create( servers[repo] )
                    .setPath( Jisho.filename().getAbsolutePath() )
                    .setListener( new DatabaseUpdateListener() )
                    .start();
            return;
        }

        if( App.preferences().getBoolean(
                App.resources().getString( R.string.settings_enable_check_database_update ), true ) )
        {
            Log.i( "TAG", "Verificando por atualizações do banco de dados" );
            //FileDownloader.getImpl().
            //        create( UPDATE_URL )
            //        .setPath( Jisho.filename().getAbsolutePath() )
            //.setListener( new )
            //        .start();
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        getWindow().setFlags(
                !App.preferences().getBoolean( App.resources().getString( R.string.settings_keep_screen_on ), false ) ? 0 : WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON );

        updateFavoriteIcon();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        FileDownloader.getImpl().unBindService();
        System.gc();
    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = (DrawerLayout) findViewById( R.id.drawer );

        if( drawer.isDrawerOpen( GravityCompat.START ) )
        {
            drawer.closeDrawer( GravityCompat.START );
            return;
        }

        if( doubleClick )
        {
            int pid = android.os.Process.myPid();
            super.onBackPressed();
            FileDownloader.getImpl().unBindService();
            android.os.Process.killProcess( pid );
            return;
        }

        doubleClick = true;
        App.show( "Pressione novamente para sair" );

        new Handler().postDelayed( new Runnable()
        {
            @Override
            public void run()
            {
                doubleClick = false;
            }
        }, 2000 );
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate( R.menu.main, menu );

        favButton = menu.findItem( R.id.favoritar );
        searchButton = menu.findItem( R.id.pesquisar );
        updateButton = menu.findItem( R.id.atualizar );

        updateFavoriteIcon();

        return true;
    }

    private void showProverbOfDay()
    {
        Kotowaza kotowaza = Jisho.kotowaza();
        if( kotowaza != null )
        {
            View header = navLeft.getHeaderView( 0 );
            ((TextView) header.findViewById( R.id.kotowaza )).setText( kotowaza.kanji() );
            ((TextView) header.findViewById( R.id.romaji )).setText( kotowaza.romaji() );
            ((TextView) header.findViewById( R.id.eigo )).setText( kotowaza.eigo() );
        }
    }

    private void updateFavoriteIcon()
    {
        Kanji k = adapter.getItemAtPosition( pager.getCurrentItem() );
        if( k != null )
        {
            updateFavoriteIcon( favoritos.contains( k.kanji() ) );
        }
    }

    private void updateFavoriteIcon( boolean value )
    {
        if( favButton != null )
        {
            favButton.setIcon( value ? R.drawable.like : R.drawable.unlike );
        }
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        int id = item.getItemId();

        if( id == android.R.id.home )
        {
            DrawerLayout drawer = (DrawerLayout) findViewById( R.id.drawer );

            if( !drawer.isDrawerOpen( GravityCompat.START ) )
            {
                drawer.openDrawer( GravityCompat.START );
            }
            else
            {
                drawer.closeDrawer( GravityCompat.START );
            }
        }
        else if( id == R.id.pesquisar )
        {
            if( !searchBoxIsVisible )
            {
                ActionBar actionBar = getSupportActionBar();
                actionBar.setDisplayShowCustomEnabled( true );
                actionBar.setCustomView( R.layout.kanji_searchbar );

                searchBox = (EditText) actionBar.getCustomView().findViewById( R.id.searchbox );
                kanjiCount = (TextView) actionBar.getCustomView().findViewById( R.id.search_count );
                kanjiCount.setText( String.valueOf( adapter.getCount() ) );

                searchBox.removeTextChangedListener( adapter );
                searchBox.setText( searchText );
                searchBox.addTextChangedListener( adapter );
                searchBox.requestFocus();

                searchButton.setIcon( R.drawable.close );

                searchBoxIsVisible = true;
            }
            else
            {
                getSupportActionBar().setDisplayShowCustomEnabled( false );
                searchButton.setIcon( R.drawable.search );
                searchBoxIsVisible = false;
            }
        }
        else if( id == R.id.favoritar )
        {
            Kanji kanji = adapter.getItemAtPosition( pager.getCurrentItem() );

            if( kanji != null )
            {
                boolean ehFavorito = favoritos.contains( kanji.kanji() );

                if( ehFavorito )
                {
                    favoritos.remove( kanji.kanji() );
                    updateFavoriteIcon( false );
                }
                else
                {
                    favoritos.add( kanji.kanji() );
                    updateFavoriteIcon( true );
                }
            }
        }
        else if( id == R.id.atualizar )
        {
            new BaseDialog( this,
                            "Atualização",
                            "Você deseja atualizar o banco de dados?",
                            App.resources().getString( android.R.string.no ),
                            App.resources().getString( android.R.string.yes ),
                            new BaseDialog.OnButtonClickListener()
                            {
                                @Override
                                public void onButtonClick( int button )
                                {
                                    if( button == BaseDialog.POSITIVE_BUTTON )
                                    {
                                        if( Jisho.filename().exists() )
                                        {
                                            Jisho.filename().delete();
                                        }

                                        int repo = Integer.parseInt( App.preferences().getString(
                                                App.resources().getString( R.string.settings_choose_database_repo ), "0" ) );

                                        FileDownloader.getImpl().
                                                create( servers[repo] )
                                                .setPath( Jisho.filename().getAbsolutePath() )
                                                .setListener( new DatabaseUpdateListener() )
                                                .start();

                                    }
                                }
                            } ).show();
        }

        return true;
    }

    public void databaseUpdated()
    {
        adapter.notifyDataSetChanged();
        pager.setAdapter( adapter );
        updateFavoriteIcon();
    }

    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data )
    {
        if( requestCode == SETTINGS_PREFS_RC )
        {
            if( resultCode == SettingsActivity.OK )
            {
                databaseUpdated();
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected( MenuItem item )
    {
        int id = item.getItemId();

        if( id == R.id.favoritos )
        {
            Intent i = new Intent( this, FavoritosActivity.class );
            startActivity( i );
        }
        else if( id == R.id.config )
        {
            Intent i = new Intent( this, SettingsActivity.class );
            startActivityForResult( i, SETTINGS_PREFS_RC );
        }
        else if( id == R.id.info )
        {
            BaseDialog.create( this, "Sobre", App.version() ).show();
            return true;
        }
        else if( id == R.id.filter )
        {
            new FilterDialog( this, new BaseDialog.OnButtonClickListener()
            {
                @Override
                public void onButtonClick( int button )
                {
                    if( button == BaseDialog.POSITIVE_BUTTON )
                    {
                        databaseUpdated();
                    }
                }
            } ).show();

        }

        ((DrawerLayout) findViewById( R.id.drawer )).closeDrawer( GravityCompat.START );

        return true;
    }

    private class KanjiPagerAdapter extends FragmentStatePagerAdapter implements TextWatcher
    {
        public List<Kanji> kanjis;

        public KanjiPagerAdapter( FragmentManager fm )
        {
            super( fm );

            kanjis = Jisho.kanjis( searchText,
                                   App.preferences().getInt( App.JLPT_FILTER, 0x1F ),
                                   App.preferences().getInt( App.GRADE_FILTER, 0x7F )
                                 );
        }

        @Override
        public Fragment getItem( int position )
        {
            KanjiFragment fragment = new KanjiFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable( "KANJI", getItemAtPosition( position ) );
            fragment.setArguments( bundle );

            return fragment;
        }

        public Kanji getItemAtPosition( int position )
        {
            return kanjis.size() > 0 ? kanjis.get( position ) : null;
        }

        @Override
        public int getItemPosition( Object object )
        {
            return 0;
        }

        @Override
        public void notifyDataSetChanged()
        {
            //Obtém a lista de kanjis.
            kanjis = Jisho.kanjis( searchText,
                                   App.preferences().getInt( App.JLPT_FILTER, 0x1F ),
                                   App.preferences().getInt( App.GRADE_FILTER, 0x7F )
                                 );
        }

        @Override
        public int getCount()
        {
            return kanjis.size();
        }

        @Override
        public void beforeTextChanged( CharSequence s, int start, int count, int after )
        {
        }

        @Override
        public void onTextChanged( CharSequence s, int start, int before, int count )
        {
        }

        @Override
        public void afterTextChanged( Editable s )
        {
            searchText = s.length() == 0 ? null : s.toString();

            databaseUpdated();

            if( kanjiCount != null )
            {
                kanjiCount.setText( String.valueOf( kanjis.size() ) );
            }
        }
    }

    private class CheckForDatabaseUpdates extends FileDownloadSampleListener
    {

        public CheckForDatabaseUpdates()
        {
        }

        @Override
        protected void completed( BaseDownloadTask task )
        {

            super.completed( task );
        }

        protected Boolean doInBackground( String... params )
        {
            //Recupera um array (ASCII) no seguinte formato:
            //AAAAMMDD
            //A - ano, M = mes, D - dia
            //return DownloadTask.download( UPDATE_URL, dados );
            return false;
        }

        protected void onPostExecute( Boolean result )
        {
            if( result )
            {
                long time = -1;

                try
                {
                    //time = Long.parseLong( new String( dados ) );
                }
                catch( Exception e )
                {
                    e.printStackTrace();
                }

                if( time != -1 && time > Jisho.versao() )
                {
                    Log.i( "TAG", "Há uma nova atualização: " + time );
                    if( updateButton != null )
                    {
                        updateButton.setIcon( R.drawable.update_notification );
                        MenuItemCompat.setShowAsAction( updateButton, MenuItemCompat.SHOW_AS_ACTION_ALWAYS );
                    }
                }
                else
                {
                    Log.i( "TAG", "Não há atualizações disponiveis" );
                }
            }
            else
            {
                Log.i( "TAG", "Erro ao atualizar o banco de dados." );
            }
        }
    }

    private class DatabaseUpdateListener extends FileDownloadSampleListener
    {
        private final Intent notificationIntent;
        private final PendingIntent pendingIntent;

        public DatabaseUpdateListener()
        {
            notificationIntent = new Intent( MainActivity.this, MainActivity.class );
            pendingIntent = PendingIntent.getActivity( MainActivity.this, 0, notificationIntent, 0 );
        }

        private void setMessage( String message, boolean alerting )
        {
            NotificationCompat.Builder builder = new NotificationCompat.Builder( MainActivity.this )
                    .setSmallIcon( R.drawable.download )
                    .setContentTitle( "JKanji" )
                    .setContentText( message )
                    .setContentIntent( pendingIntent );

            Notification notification = builder.build();

            if( alerting )
            {
                notification.defaults |= Notification.DEFAULT_VIBRATE;
                notification.defaults |= Notification.DEFAULT_SOUND;
            }

            notification.flags |= Notification.FLAG_AUTO_CANCEL;

            NotificationManager notificationManager =
                    (NotificationManager) MainActivity.this.getSystemService( Context.NOTIFICATION_SERVICE );
            notificationManager.notify( 0, notification );
        }

        @Override
        protected void connected( BaseDownloadTask task, String etag, boolean isContinue, int soFarBytes, int totalBytes )
        {
            setMessage( "Download iniciou", true );
            super.connected( task, etag, isContinue, soFarBytes, totalBytes );
        }

        @Override
        protected void completed( BaseDownloadTask task )
        {
            setMessage( "Download concluído", true );

            updateButton.setIcon( R.drawable.update );
            updateButton.setTitle( "Atualizar" );
            MenuItemCompat.setShowAsAction( updateButton, MenuItemCompat.SHOW_AS_ACTION_NEVER );
            databaseUpdated();

            super.completed( task );
        }

        @Override
        protected void progress( BaseDownloadTask task, int soFarBytes, int totalBytes )
        {
            int p = (soFarBytes * 100) / totalBytes;
            if( p >= 0 && p <= 100 )
            {
                setMessage( String.format( "Baixando: %d%%", p ), false );
            }
            super.progress( task, soFarBytes, totalBytes );
        }

        @Override
        protected void error( BaseDownloadTask task, Throwable e )
        {
            setMessage( "Erro ao baixar", true );
            e.printStackTrace();
            super.error( task, e );
        }
    }
}
