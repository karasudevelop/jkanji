package com.karasudevelop.jkanji;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class KanjiView extends FrameLayout
{
    private Kanji kanji;
    private int count = 10;

    public KanjiView( Context context )
    {
        this( context, null );
    }

    public KanjiView( Context context, AttributeSet attrs )
    {
        super( context, attrs );

        LayoutInflater li = (LayoutInflater) getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View view = li.inflate( R.layout.kanji_view, this, false );
        addView( view );

        ((TextView) view.findViewById( R.id.kanji )).setTypeface( App.fonts().get( FontManager.KANJISTROKEORDERS ) );

        view.findViewById( R.id.mais ).setOnClickListener( new OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                count += 10;
                showKotobas( count );
            }
        } );
    }

    public void set( Kanji kanji )
    {
        this.kanji = kanji;

        if( kanji != null )
        {
            View child = getChildAt( 0 );

            ((TextView) child.findViewById( R.id.kanji )).setText( kanji.kanji() );
            ((TextView) child.findViewById( R.id.meaning )).setText( kanji.meaning() );
            child.findViewById( R.id.on ).setVisibility( (kanji.on() != null && kanji.on().length() > 0) ? VISIBLE : GONE );
            child.findViewById( R.id.kun ).setVisibility( (kanji.kun() != null && kanji.kun().length() > 0) ? VISIBLE : GONE );
            ((TextView) child.findViewById( R.id.on )).setText( kanji.on() );
            ((TextView) child.findViewById( R.id.kun )).setText( kanji.kun() );
            ((TextView) child.findViewById( R.id.jlpt )).setText( kanji.jlpt() > 0 ? String.valueOf( kanji.jlpt() ) : "" );
            ((TextView) child.findViewById( R.id.grade )).setText( kanji.grade() == 9999 ? "" : String.valueOf( kanji.grade() ) );

            showKotobas( 10 );
        }
    }

    public void showKotobas( int number )
    {
        List<Kotoba> kotobas = Jisho.kotobas( kanji.kanji(), number );

        View child = getChildAt( 0 );

        ((ListView) child.findViewById( R.id.palavras )).setAdapter(
                new KanjiViewAdapter( getContext(), kotobas ) );

        Log.i( "TAG", number + "," + kotobas.size() );

        if( number != kotobas.size() )
        {
            child.findViewById( R.id.mais ).setVisibility( INVISIBLE );
        }
    }

    private class KanjiViewAdapter extends ArrayAdapter<Kotoba>
    {
        private final List<View> views = new ArrayList<>();

        public KanjiViewAdapter( Context context, List<Kotoba> words )
        {
            super( context, -1, words );

            for( int i = 0; i < words.size(); i++ )
            {
                KotobaView kotobaView = new KotobaView( getContext() );
                kotobaView.set( words.get( i ) );
                views.add( kotobaView );
            }
        }

        @Override
        public View getView( int position, View convertView, ViewGroup parent )
        {
            return views.get( position );
        }
    }
}
