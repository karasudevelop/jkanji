package com.karasudevelop.jkanji;

public class JapaneseHelper
{
    public static boolean isKatakana( char c )
    {
        return c >= 0x30A0 && c <= 0x30FF;
    }

    public static boolean isHiragana( char c )
    {
        return c >= 0x3040 && c <= 0x309F;
    }

    public static boolean isKana( char c )
    {
        return isHiragana( c ) || isKatakana( c );
    }

    public static boolean isKanji( char c )
    {
        return c >= 0x4E00 && c <= 0x9FAF ||
               c >= 0x3400 && c <= 0x4DBF;
    }

    public static boolean isJapanese( char c )
    {
        return isKana( c ) || isKanji( c );
    }
}
