package com.karasudevelop.jkanji;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

//Representa uma janela de diálogo utilizada para configurar os filtros de JLPT e Grade.
public class FilterDialog extends BaseDialog
{
    private final CheckBox[] niveis = new CheckBox[5];
    private final CheckBox[] grades = new CheckBox[9];
    private final Button kyouiku, jouyou;

    public FilterDialog( Context context,
                         OnButtonClickListener onButtonClickListener )
    {
        super( context,
               null,
               R.layout.filter_dialog,
               App.resources().getString( android.R.string.cancel ),
               App.resources().getString( android.R.string.ok ),
               onButtonClickListener );

        niveis[0] = (CheckBox) container().findViewById( R.id.n5 );
        niveis[1] = (CheckBox) container().findViewById( R.id.n4 );
        niveis[2] = (CheckBox) container().findViewById( R.id.n3 );
        niveis[3] = (CheckBox) container().findViewById( R.id.n2 );
        niveis[4] = (CheckBox) container().findViewById( R.id.n1 );

        grades[0] = (CheckBox) container().findViewById( R.id.G1 );
        grades[1] = (CheckBox) container().findViewById( R.id.G2 );
        grades[2] = (CheckBox) container().findViewById( R.id.G3 );
        grades[3] = (CheckBox) container().findViewById( R.id.G4 );
        grades[4] = (CheckBox) container().findViewById( R.id.G5 );
        grades[5] = (CheckBox) container().findViewById( R.id.G6 );
        grades[6] = (CheckBox) container().findViewById( R.id.G78 ); //Junior High School
        grades[7] = (CheckBox) container().findViewById( R.id.G9 );
        grades[8] = (CheckBox) container().findViewById( R.id.G10 );

        kyouiku = (Button) container().findViewById( R.id.kyouiku ); //Grade 1 ao 6
        jouyou = (Button) container().findViewById( R.id.jouyou ); //Grade 1 ao 8

        kyouiku.setOnClickListener( onClickListener );
        jouyou.setOnClickListener( onClickListener );

        jlpt( App.preferences().getInt( App.JLPT_FILTER, 0x1F ) );
        grade( App.preferences().getInt( App.GRADE_FILTER, 0x7F ) ); //jouyou kanji.
    }

    private final View.OnClickListener onClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick( View v )
        {
            grade( v == kyouiku ? 0x3F : 0x7F );
        }
    };

    public void jlpt( int value )
    {
        for( CheckBox nivel : niveis )
        {
            nivel.setChecked( (value & 1) == 1 );
            value >>= 1;
        }
    }

    public void grade( int grade )
    {
        for( CheckBox g : grades )
        {
            g.setChecked( (grade & 1) == 1 );
            grade >>= 1;
        }
    }

    public int jlpt()
    {
        int result = 0;

        for( int i = niveis.length - 1; i >= 0; i-- )
        {
            result <<= 1;
            result |= niveis[i].isChecked() ? 1 : 0;
        }

        return result;
    }

    public int grade()
    {
        int result = 0;

        for( int i = grades.length - 1; i >= 0; i-- )
        {
            result <<= 1;
            result |= grades[i].isChecked() ? 1 : 0;
        }

        return result;
    }

    @Override
    public void OnPositiveClick()
    {
        SharedPreferences.Editor editor = App.editor();

        editor.putInt( App.JLPT_FILTER, jlpt() & 0x1F );
        editor.putInt( App.GRADE_FILTER, grade() & 0x1FF );
        editor.apply();

        super.OnPositiveClick();
    }

}