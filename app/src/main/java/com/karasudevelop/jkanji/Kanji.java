package com.karasudevelop.jkanji;

import java.io.Serializable;

//http://www.edrdg.org/jmdict/j_jmdict.html
public class Kanji implements Serializable
{
    private static final long serialVersionUID = 3L;

    private String kanji;
    private String meaning;
    private String on;
    private String kun;
    private String nanori;
    private int jlpt;
    private int grade;
    private int frequency;
    private int strokeCount;

    public Kanji( String kanji,
                  String on,
                  String kun,
                  String nanori,
                  int jlpt,
                  int grade,
                  int frequency,
                  int strokeCount,
                  String meaning )
    {
        this.kanji = kanji;
        this.on = on;
        this.kun = kun;
        this.nanori = nanori;
        this.jlpt = jlpt;
        this.grade = grade;
        this.frequency = frequency;
        this.strokeCount = strokeCount;
        this.meaning = meaning;
    }

    public String kanji()
    {
        return kanji;
    }

    public String meaning()
    {
        return meaning;
    }

    public String on()
    {
        return on;
    }

    public String kun()
    {
        return kun;
    }

    public String nanori()
    {
        return nanori;
    }

    public int jlpt()
    {
        return jlpt;
    }

    public int grade()
    {
        return grade;
    }

    public int frequency()
    {
        return frequency;
    }

    public int strokeCount()
    {
        return strokeCount;
    }
}