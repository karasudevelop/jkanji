package com.karasudevelop.jkanji;

public class Kotowaza
{
    private final String kanji;
    private final String romaji;
    private final String eigo;

    public Kotowaza( String kanji, String romaji, String eigo )
    {
        this.kanji = kanji;
        this.romaji = romaji;
        this.eigo = eigo;
    }

    public String kanji()
    {
        return kanji;
    }

    public String romaji()
    {
        return romaji;
    }

    public String eigo()
    {
        return eigo;
    }
}
