package com.karasudevelop.jkanji;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class KanjiFragment extends Fragment
{
    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        ViewGroup rootView = (ViewGroup) inflater.inflate( R.layout.kanji_fragment, container, false );

        try
        {
            KanjiView view = (KanjiView) rootView.findViewById( R.id.kanji );
            view.set( (Kanji) getArguments().getSerializable( "KANJI" ) );
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }

        return rootView;
    }
}
