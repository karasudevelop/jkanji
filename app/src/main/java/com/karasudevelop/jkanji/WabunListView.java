package com.karasudevelop.jkanji;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class WabunListView extends LinearLayout
{
    private final ListView container;

    public WabunListView( Context context )
    {
        this( context, null );
    }

    public WabunListView( Context context, AttributeSet attrs )
    {
        super( context, attrs );

        container = new ListView( context );
        container.setLayoutParams( new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT ) );
        addView( container );
    }

    public void set( List<Wabun> wabuns )
    {
        if( wabuns != null )
        {
            container.setAdapter( new WabunListViewAdapter( getContext(), wabuns ) );
        }
    }

    private class WabunListViewAdapter extends ArrayAdapter<Wabun>
    {
        private final Context context;
        private final List<Wabun> wabuns;

        public WabunListViewAdapter( Context context, List<Wabun> wabuns )
        {
            super( context, -1, wabuns );
            this.context = context;
            this.wabuns = wabuns;
        }

        @Override
        public View getView( int position, View convertView, ViewGroup parent )
        {
            View view;
            LayoutInflater li = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

            if( convertView == null )
            {
                view = li.inflate( R.layout.wabun_view, parent, false );
            }
            else
            {
                view = convertView;
            }

            ((TextView) view.findViewById( R.id.kanji )).setText( wabuns.get( position ).kanji() );
            ((TextView) view.findViewById( R.id.eigo )).setText( wabuns.get( position ).eigo() );

            return view;
        }
    }
}
