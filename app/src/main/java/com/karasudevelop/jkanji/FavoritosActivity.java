package com.karasudevelop.jkanji;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import java.util.List;

//Classe utilizada para exibir todos os kanjis favoritos.
public class FavoritosActivity extends BaseActivity implements
                                                    NavigationView.OnNavigationItemSelectedListener
{
    private NavigationView nav;
    private ViewPager pager;
    private DrawerLayout drawer;

    private KanjiPagerAdapter adapter;

    private Favoritos favoritos;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        setContentView( R.layout.favoritos_activity );

        ActionBar actionBar = getSupportActionBar();

        if( actionBar != null )
        {
            actionBar.setTitle( "Favoritos" );
            actionBar.setHomeAsUpIndicator( R.drawable.menu_white );
            actionBar.setDisplayHomeAsUpEnabled( true );
        }

        drawer = (DrawerLayout) findViewById( R.id.drawer );
        pager = (ViewPager) findViewById( R.id.pager );
        nav = (NavigationView) findViewById( R.id.nav_left );

        nav.setNavigationItemSelectedListener( this ); //Evento de item selecionado.
        nav.setItemIconTintList( null ); //Habilita a cor dos icones.

        favoritos = new Favoritos( this );

        adapter = new KanjiPagerAdapter( getSupportFragmentManager() );

        pager.setAdapter( adapter );
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        getWindow().setFlags(
                !App.preferences().getBoolean(
                        App.resources().getString( R.string.settings_keep_screen_on ), false ) ? 0 : WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON );
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        System.gc();
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        getMenuInflater().inflate( R.menu.favoritos, menu );

        return true;
    }

    @Override
    public boolean onNavigationItemSelected( MenuItem item )
    {
        int id = item.getItemId();

        if( id == R.id.config )
        {
        }
        else if( id == R.id.info )
        {
            BaseDialog.create( this, "Sobre", App.version() ).show();

            return true;
        }

        drawer.closeDrawer( GravityCompat.START );

        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        int id = item.getItemId();

        if( id == android.R.id.home )
        {
            if( !drawer.isDrawerOpen( GravityCompat.START ) )
            {
                drawer.openDrawer( GravityCompat.START );
            }
            else
            {
                drawer.closeDrawer( GravityCompat.START );
            }
        }
        else if( id == R.id.remove )
        {
            if( adapter.getCount() > 0 )
            {
                favoritos.remove(
                        adapter.getItemAtPosition( pager.getCurrentItem() ).kanji() );
                adapter.notifyDataSetChanged();
                pager.setAdapter( adapter );
            }
        }
        else if( id == R.id.remove_all )
        {
            if( adapter.getCount() > 0 )
            {
                favoritos.clear();
                adapter.notifyDataSetChanged();
                pager.setAdapter( adapter );
            }
        }

        return true;
    }

    private class KanjiPagerAdapter extends FragmentStatePagerAdapter
    {
        public List<String> kanjis;

        public KanjiPagerAdapter( FragmentManager fm )
        {
            super( fm );

            kanjis = favoritos.all();
        }

        @Override
        public Fragment getItem( int position )
        {
            KanjiFragment fragment = new KanjiFragment();

            Bundle bundle = new Bundle();
            bundle.putSerializable( "KANJI", getItemAtPosition( position ) );
            fragment.setArguments( bundle );

            return fragment;
        }

        public Kanji getItemAtPosition( int position )
        {
            return Jisho.kanji( kanjis.get( position ) );
        }

        @Override
        public int getItemPosition( Object object )
        {
            return 0;
        }

        @Override
        public void notifyDataSetChanged()
        {
            kanjis = favoritos.all();
        }

        @Override
        public int getCount()
        {
            return kanjis.size();
        }
    }
}
