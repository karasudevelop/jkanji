package com.karasudevelop.jkanji;

import android.app.Application;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

public class FontManager
{
    private final Map<String, Typeface> fonts = new HashMap<>();

    public static final String KANJISTROKEORDERS = "KANJISTROKEORDERS";

    public FontManager( Application app )
    {
        fonts.put( KANJISTROKEORDERS,
                   Typeface.createFromAsset( app.getAssets(), "fonts/KanjiStrokeOrders.ttf" ) );
    }

    public Typeface get( String key )
    {
        return fonts.get( key );
    }
}
