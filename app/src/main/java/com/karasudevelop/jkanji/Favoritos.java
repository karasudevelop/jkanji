package com.karasudevelop.jkanji;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class Favoritos extends SQLiteOpenHelper
{
    //Constantes.
    public static final String NAME = "favoritos.db";
    public static final int VERSION = 2;
    public static final String KANJIS_TABLE_NAME = "kanjis";

    public Favoritos( Context contexto )
    {
        super( contexto, NAME, null, VERSION );
    }

    @Override
    public void onCreate( SQLiteDatabase db )
    {
        final String CREATE = "CREATE TABLE '" + KANJIS_TABLE_NAME + "' (" +
                              " 'id' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE," +
                              " 'value' TEXT NOT NULL" +
                              ");";
        db.execSQL( CREATE );
    }

    public void add( String c )
    {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put( "value", c );

        db.insert( KANJIS_TABLE_NAME, null, values );
        db.close();
    }

    public void remove( String c )
    {
        SQLiteDatabase db = getWritableDatabase();
        db.delete( KANJIS_TABLE_NAME, "value = ?", new String[]{ c } );
        db.close();
    }

    public void clear()
    {
        SQLiteDatabase db = getWritableDatabase();
        db.delete( KANJIS_TABLE_NAME, null, null );
        db.close();
    }

    public boolean contains( String k )
    {
        boolean result = false;

        if( k == null )
        {
            return false;
        }

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery( "SELECT * FROM " + KANJIS_TABLE_NAME + " WHERE value = ?", new String[]{ k } );

        if( cursor != null )
        {
            result = cursor.getCount() > 0;
            cursor.close();
        }

        db.close();

        return result;
    }

    public List<String> all()
    {
        List<String> result = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery( "SELECT * FROM " + KANJIS_TABLE_NAME, null );

        if( cursor != null && cursor.moveToFirst() )
        {
            do
            {
                result.add( cursor.getString( 1 ) );
            } while( cursor.moveToNext() );

            cursor.close();
        }

        db.close();

        return result;
    }

    @Override
    public void onUpgrade( SQLiteDatabase db, int oldVersion, int newVersion )
    {
        if( oldVersion != newVersion )
        {
            db.execSQL( "DROP TABLE IF EXISTS " + KANJIS_TABLE_NAME );
            onCreate( db );
        }
    }

    @Override
    public void onDowngrade( SQLiteDatabase db, int oldVersion, int newVersion )
    {
    }
}