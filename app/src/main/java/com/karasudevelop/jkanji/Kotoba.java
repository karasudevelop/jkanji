package com.karasudevelop.jkanji;

import java.io.Serializable;

public class Kotoba implements Serializable
{
    private static final long serialVersionUID = 3L;

    private String kanji;
    private String writing;
    private String reading;
    private String meaning;
    private int jlpt;

    public Kotoba( String kanji,
                   String writing,
                   String reading,
                   int jlpt,
                   String meaning )
    {
        this.reading = reading;
        this.kanji = kanji;
        this.writing = writing;
        this.meaning = meaning;
        this.jlpt = jlpt;
    }

    public String kanji()
    {
        return kanji;
    }

    public String writing()
    {
        return writing;
    }

    public String reading()
    {
        return reading;
    }

    public String meaning()
    {
        return meaning;
    }

    public int jlpt()
    {
        return jlpt;
    }
}
