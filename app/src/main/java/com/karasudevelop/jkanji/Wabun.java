package com.karasudevelop.jkanji;

public class Wabun
{
    private final String kanji;
    private final String eigo;

    public Wabun( String kanji, String eigo )
    {
        this.eigo = eigo;
        this.kanji = kanji;
    }

    public String kanji()
    {
        return kanji;
    }

    public String eigo()
    {
        return eigo;
    }
}
