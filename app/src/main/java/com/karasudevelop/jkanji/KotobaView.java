package com.karasudevelop.jkanji;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class KotobaView extends FrameLayout
{
    private Kotoba kotoba;

    public KotobaView( Context context )
    {
        this( context, null );
    }

    public KotobaView( Context context, AttributeSet attrs )
    {
        super( context, attrs );

        LayoutInflater li = (LayoutInflater) getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View v = li.inflate( R.layout.kotoba_view, this, false );
        addView( v );

        setOnClickListener( new OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                WabunListView wabunListView = new WabunListView( getContext() );
                List<Wabun> wabuns = Jisho.wabuns( kotoba.kanji() );
                wabunListView.set( wabuns );

                new BaseDialog( getContext(),
                                kotoba.kanji() + " (" + wabuns.size() + ")",
                                wabunListView,
                                null,
                                null,
                                null ).show();
            }
        } );
    }

    public void set( Kotoba kotoba )
    {
        this.kotoba = kotoba;

        if( kotoba != null )
        {
            View v = getChildAt( 0 );

            ((TextView) v.findViewById( R.id.kana )).setText( kotoba.reading() );
            ((TextView) v.findViewById( R.id.meaning )).setText( kotoba.meaning() );
            v.findViewById( R.id.jlpt ).setBackgroundColor( App.resources().getIntArray( R.array.jlpt )[kotoba.jlpt()] );
            ((TextView) v.findViewById( R.id.writing )).setText( kotoba.writing() );
            v.findViewById( R.id.writing ).setVisibility(
                    App.preferences().getBoolean( App.resources().getString( R.string.settings_show_kanji_ext ), true ) &&
                    kotoba.writing() != null && kotoba.writing().length() > 0 ? VISIBLE : GONE );
            LinearLayout word = (LinearLayout) v.findViewById( R.id.word );
            word.removeAllViews();

            LayoutInflater li = (LayoutInflater) getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );

            for( int i = 0; i < kotoba.kanji().length(); i++ )
            {
                final char c = kotoba.kanji().charAt( i );

                TextView caracter = (TextView) li.inflate( R.layout.kotoba_word, word, false );
                caracter.setText( String.valueOf( c ) );

                if( JapaneseHelper.isKanji( c ) )
                {
                    caracter.setOnClickListener( new OnClickListener()
                    {
                        @Override
                        public void onClick( View v )
                        {
                            KanjiView kanjiView = new KanjiView( getContext() );
                            kanjiView.set( Jisho.kanji( String.valueOf( c ) ) );

                            new BaseDialog( getContext(),
                                            String.valueOf( c ),
                                            kanjiView,
                                            null,
                                            null,
                                            null ).show();
                        }
                    } );
                }

                word.addView( caracter );
            }
        }
    }
}
