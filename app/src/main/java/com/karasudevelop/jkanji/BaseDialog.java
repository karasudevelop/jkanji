package com.karasudevelop.jkanji;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

public class BaseDialog
{
    private AlertDialog.Builder builder;
    private Dialog dialog;
    private Context context;
    private View container;
    private OnButtonClickListener onButtonClickListener;

    public static final int POSITIVE_BUTTON = 1;
    public static final int NEGATIVE_BUTTON = 0;

    protected BaseDialog( Context context )
    {
        this.context = context;
        builder = new AlertDialog.Builder( context );
    }

    public BaseDialog( Context context,
                       String title,
                       String menssage,
                       String negativeText,
                       String positiveText,
                       OnButtonClickListener onButtonClickListener )
    {
        this( context );

        this.onButtonClickListener = onButtonClickListener;

        builder.setTitle( title );
        builder.setMessage( menssage );

        if( negativeText != null )
            builder.setNegativeButton( negativeText,
                                       this.onNegativeClick );

        if( positiveText != null )
            builder.setPositiveButton( positiveText,
                                       this.onPositiveClick );

        dialog = builder.create();
        dialog.setOnDismissListener( onDismissListener );
    }

    public BaseDialog( Context context,
                       String title,
                       @LayoutRes int layout,
                       String negativeText,
                       String positiveText,
                       OnButtonClickListener onButtonClickListener )
    {
        this( context );

        this.onButtonClickListener = onButtonClickListener;

        builder.setTitle( title );
        LayoutInflater li = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        container = li.inflate( layout, null, false );
        builder.setView( container );

        if( negativeText != null )
            builder.setNegativeButton( negativeText,
                                       this.onNegativeClick );
        if( positiveText != null )
            builder.setPositiveButton( positiveText,
                                       this.onPositiveClick );

        dialog = builder.create();
        dialog.setOnDismissListener( onDismissListener );
    }

    public BaseDialog( Context context,
                       String title,
                       View view,
                       String negativeText,
                       String positiveText,
                       OnButtonClickListener onButtonClickListener )
    {
        this( context );

        this.onButtonClickListener = onButtonClickListener;

        builder.setTitle( title );
        container = view;
        builder.setView( container );

        if( negativeText != null )
            builder.setNegativeButton( negativeText,
                                       this.onNegativeClick );
        if( positiveText != null )
            builder.setPositiveButton( positiveText,
                                       this.onPositiveClick );

        dialog = builder.create();
        dialog.setOnDismissListener( onDismissListener );
    }

    private final DialogInterface.OnClickListener onNegativeClick = new DialogInterface.OnClickListener()
    {
        @Override
        public void onClick( DialogInterface dialog, int which )
        {
            OnNegativeClick();
        }
    };

    private final DialogInterface.OnClickListener onPositiveClick = new DialogInterface.OnClickListener()
    {
        @Override
        public void onClick( DialogInterface dialog, int which )
        {
            OnPositiveClick();
        }
    };

    private final DialogInterface.OnDismissListener onDismissListener = new DialogInterface.OnDismissListener()
    {
        @Override
        public void onDismiss( DialogInterface dialog )
        {
            OnDismiss();
        }
    };

    public static BaseDialog create( Context context,
                                     String title,
                                     String message )
    {
        return new BaseDialog(
                context,
                title,
                message,
                null,
                App.resources().getString( android.R.string.ok ),
                null );
    }

    public final Context context()
    {
        return context;
    }

    public final AlertDialog.Builder builder()
    {
        return builder;
    }

    public final Dialog dialog()
    {
        return dialog;
    }

    public final View container()
    {
        return container;
    }

    public void show()
    {
        dialog.show();
    }

    public void hide()
    {
        dialog.hide();
    }

    public BaseDialog resize( int width, int height )
    {
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom( dialog.getWindow().getAttributes() );
        lp.width = width;
        lp.height = height;

        dialog.getWindow().setAttributes( lp );
        
        return this;
    }

    protected void OnNegativeClick()
    {
        if( onButtonClickListener != null )
        {
            onButtonClickListener.onButtonClick( NEGATIVE_BUTTON );
        }
    }

    protected void OnPositiveClick()
    {
        if( onButtonClickListener != null )
        {
            onButtonClickListener.onButtonClick( POSITIVE_BUTTON );
        }
    }

    protected void OnDismiss()
    {
    }

    public interface OnButtonClickListener
    {
        void onButtonClick( int button );
    }
}
