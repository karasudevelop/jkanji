package com.karasudevelop.jkanji;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class SettingsActivity extends PreferenceActivity implements
                                                         SharedPreferences.OnSharedPreferenceChangeListener
{
    //Constantes.
    public static final int OK = 0;
    public static final int CANCELED = 1;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        addPreferencesFromResource( R.xml.settings );

        setResult( CANCELED );
        PreferenceManager.getDefaultSharedPreferences( this ).
                registerOnSharedPreferenceChangeListener( this );
    }

    @Override
    public void onSharedPreferenceChanged( SharedPreferences sharedPreferences, String key )
    {
        if( key == App.resources().getString( R.string.settings_random_get_kanjis ) ||
            key == App.resources().getString( R.string.settings_show_kanji_ext ) )
        {
            setResult( OK );
        }
    }

    @Override
    protected void onDestroy()
    {
        PreferenceManager.getDefaultSharedPreferences( this ).
                unregisterOnSharedPreferenceChangeListener( this );
        super.onDestroy();
    }
}
